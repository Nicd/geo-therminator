# GeoTherminator

GeoTherminator allows you to see your Thermia ground heat pump's status at a single glance.

<img src="demo.png" alt="Demo image" />

I wrote this for use with my Thermia Calibra 7 pump. I don't know if it works with anything else.

## Disclaimer

**Your pump and home at risk!** Do not trust anyone with your account username and password. If you do not understand all the code contained in this software, do not use it. If you do not understand how to compile and deploy Elixir projects, do not use this. Honestly, just use the Thermia online service and you won't break your pump and void your warranty.
