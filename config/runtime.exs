import Config
import GeoTherminator.ConfigHelpers

Application.app_dir(:geo_therminator, "priv")
|> Path.join(".env")
|> DotenvParser.load_file()

config :geo_therminator,
  api_timeout: 30_000,
  api_auth_url:
    get_env("API_AUTH_URL", "https://thermia-auth-api.azurewebsites.net/api/v1/Jwt/login"),
  api_installations_url:
    get_env(
      "API_INSTALLATIONS_URL",
      "https://online-genesis-serviceapi.azurewebsites.net/api/v1/installationsInfo"
    ),
  api_device_url:
    get_env(
      "API_DEVICE_URL",
      "https://online-genesis-serviceapi.azurewebsites.net/api/v1/installations/{id}"
    ),
  api_device_status_url:
    get_env(
      "API_DEVICE_STATUS_URL",
      "https://online-genesis-serviceapi.azurewebsites.net/api/v1/installationstatus/{id}/status"
    ),
  api_device_register_url:
    get_env(
      "API_DEVICE_REGISTER_URL",
      "https://online-genesis-serviceapi.azurewebsites.net/api/v1/Registers/Installations/{id}/Groups/REG_GROUP_TEMPERATURES"
    ),
  api_device_opstat_url:
    get_env(
      "API_DEVICE_OPSTAT_URL",
      "https://online-genesis-serviceapi.azurewebsites.net/api/v1/Registers/Installations/{id}/Groups/REG_GROUP_OPERATIONAL_STATUS"
    ),
  api_opstat_mapping: %{
    # "REG_VALUE_STATUS_MANUAL" => 1,
    1 => :hand_operated,
    # "REG_VALUE_STATUS_TAPWATER" => 3,
    3 => :hot_water,
    # "REG_VALUE_STATUS_HEAT" => 4,
    4 => :heating,
    # "REG_VALUE_STATUS_COOL" => 5,
    5 => :active_cooling,
    # "REG_VALUE_STATUS_POOL" => 6,
    6 => :pool,
    # "REG_VALUE_STATUS_LEGIONELLA" => 7,
    7 => :anti_legionella,
    # "REG_VALUE_STATUS_PASSIVE_COOL" => 8,
    8 => :passive_cooling,
    # "REG_VALUE_STATUS_STANDBY" => 98,
    98 => :standby,
    # "REG_VALUE_STATUS_IDLE" => 99,
    99 => :idle,
    # "REG_VALUE_STATUS_OFF" => 100
    100 => :off
  },
  api_opstat_bitmask_mapping: %{
    1 => :hand_operated,
    2 => :defrost,
    4 => :hot_water,
    8 => :heating,
    16 => :active_cooling,
    32 => :pool,
    64 => :anti_legionella,
    128 => :passive_cooling,
    512 => :standby,
    1024 => :idle,
    2048 => :off
  },
  api_device_reg_set_url:
    get_env(
      "API_DEVICE_REG_SET_URL",
      "https://online-genesis-serviceapi.azurewebsites.net/api/v1/Registers/Installations/{id}/Registers"
    ),
  api_device_temp_set_reg_index: 3,
  api_device_reg_set_client_id: get_env("API_DEVICE_REG_SET_CLIENT_ID"),
  api_refresh: 10_000,
  b2c_client_id: get_env("B2C_CLIENT_ID", "09ea4903-9e95-45fe-ae1f-e3b7d32fa385"),
  b2c_redirect_url: get_env("B2C_REDIRECT_URL", "https://online-genesis.thermia.se/login"),
  b2c_auth_url:
    get_env(
      "B2C_AUTH_URL",
      "https://thermialogin.b2clogin.com/thermialogin.onmicrosoft.com/b2c_1a_signuporsigninonline"
    ),

  # Database directory for settings
  db_dir:
    Path.join(
      if config_target() in [:ios, :android] do
        System.get_env("HOME")
      else
        Application.app_dir(:geo_therminator, "priv")
      end,
      get_env("DB_DIR", "db")
    )

if config_env() == :dev do
  config :geo_therminator, GeoTherminatorWeb.Endpoint,
    http: [
      port: 0
    ]
end

if config_env() == :prod do
  # The secret key base is used to sign/encrypt cookies and other secrets.
  # A default value is used in config/dev.exs and config/test.exs but you
  # want to use a different value for prod and you most likely don't want
  # to check this value into version control, so we use an environment
  # variable instead.
  secret_key_base =
    System.get_env("SECRET_KEY_BASE") ||
      raise """
      environment variable SECRET_KEY_BASE is missing.
      You can generate one by calling: mix phx.gen.secret
      """

  config :geo_therminator, GeoTherminatorWeb.Endpoint,
    http: [
      # Enable IPv6 and bind on all interfaces.
      # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
      # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
      # for details about using IPv6 vs IPv4 and loopback vs public addresses.
      ip: {127, 0, 0, 1},
      port: 0
    ],
    secret_key_base: secret_key_base,
    server: true

  # ## Using releases
  #
  # If you are doing OTP releases, you need to instruct Phoenix
  # to start each relevant endpoint:
  #
  #     config :geo_therminator, GeoTherminatorWeb.Endpoint, server: true
  #
  # Then you can assemble a release by calling `mix release`.
  # See `mix help release` for more information.
end
