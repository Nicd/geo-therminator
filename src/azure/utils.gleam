import gleam/string
import gleam/list
import gleam/int
import gleam/bit_string
import helpers/binary

const alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

const alphabet_len = 62

pub fn generate_challenge(length: Int) -> String {
  let alphabet_str = bit_string.from_string(alphabet)

  list.range(0, length - 1)
  |> list.map(fn(_) { random_char(alphabet_str) })
  |> string.join(with: "")
}

fn random_char(alphabet: BitString) -> String {
  let index = int.random(0, alphabet_len)

  assert Ok(char) =
    alphabet
    |> binary.part(index, 1)
    |> bit_string.to_string()

  char
}
