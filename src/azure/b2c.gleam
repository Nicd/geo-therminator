//// Implementation of Azure B2C authentication as used by the Thermia API.
//// Mostly translated from:
//// https://github.com/klejejs/python-thermia-online-api/blob/2f0ec4e45bfecbd90932a10247283cbcd6a6c48c/ThermiaOnlineAPI/api/ThermiaAPI.py
//// Used under the Gnu General Public License 3.0
////
//// Refreshing using the refresh token is not implemented, we just get a new
//// access token every time, YOLO

import gleam/json
import gleam/base
import gleam/uri
import gleam/http
import gleam/http/request
import gleam/http/response
import gleam/dynamic
import gleam/string
import gleam/list
import gleam/result
import gleam/int
import azure/utils
import helpers/config
import helpers/crypto
import helpers/uri as uri_helpers
import helpers/parsing
import finch

const challenge_length = 43

const b2c_authorize_prefix = "var SETTINGS = "

pub type Tokens {
  Tokens(
    access_token: String,
    access_token_expires_in: Int,
    refresh_token: String,
    refresh_token_expires_in: Int,
  )
}

pub type B2CError {
  B2CError(msg: String)
}

type Cookies =
  List(#(String, String))

type AuthInfo {
  AuthInfo(state_code: String, csrf_token: String, cookies: Cookies)
}

type SelfAsserted {
  SelfAsserted(cookies: Cookies)
}

type Confirmed {
  Confirmed(code: String)
}

/// Authenticate to API with B2C authentication, returning the tokens needed
/// for using the API.
pub fn authenticate(
  username: String,
  password: String,
) -> Result(Tokens, B2CError) {
  let code_challenge = utils.generate_challenge(challenge_length)
  try auth_info = authorize(code_challenge)
  try self_asserted = signin(username, password, auth_info)
  try confirmed = confirm(self_asserted, auth_info)
  get_tokens(confirmed, code_challenge)
}

fn authorize(code_challenge: String) -> Result(AuthInfo, B2CError) {
  let auth_data = [
    #("response_type", "code"),
    #("code_challenge", hash_challenge(code_challenge)),
    #("code_challenge_method", "S256"),
    ..base_request_data()
  ]

  let req = build_req(authorize_url(), http.Get)
  let req = request.set_query(req, auth_data)

  try resp = run_req(req)

  let body_split = string.split(resp.body, "\n")
  try settings =
    list.find(
      body_split,
      fn(line) { string.starts_with(line, b2c_authorize_prefix) },
    )
    |> b2c_error("Authorize settings string not found.")

  let prefix_len = string.length(b2c_authorize_prefix)
  let settings_json =
    string.slice(settings, prefix_len, string.length(settings) - prefix_len - 2)
  try data =
    json.decode(settings_json, using: dynamic.dynamic)
    |> b2c_error(
      "Authorize settings JSON parsing error: " <> string.inspect(settings_json),
    )

  try csrf_token = data_get(data, "csrf", dynamic.string)

  try state_code_block = data_get(data, "transId", dynamic.string)
  try state_code =
    state_code_block
    |> string.split("=")
    |> list.at(1)
    |> b2c_error("State code parsing error: " <> state_code_block)

  Ok(AuthInfo(
    state_code: state_code,
    csrf_token: csrf_token,
    cookies: response.get_cookies(resp),
  ))
}

fn signin(
  username: String,
  password: String,
  auth_info: AuthInfo,
) -> Result(SelfAsserted, B2CError) {
  let self_asserted_data = [
    #("request_type", "RESPONSE"),
    #("signInName", username),
    #("password", password),
  ]

  let req =
    build_req(self_asserted_url(), http.Post)
    |> request.set_body(uri_helpers.form_urlencoded_serialize(
      self_asserted_data,
    ))
    |> request.set_query(base_query(auth_info))
    |> request.set_header("x-csrf-token", auth_info.csrf_token)

  let req =
    list.fold(
      auth_info.cookies,
      req,
      fn(acc, cookie) {
        let #(name, value) = cookie
        request.set_cookie(acc, name, value)
      },
    )

  try resp = run_req(req)

  case string.contains(resp.body, "{\"status\":\"400\"") {
    True -> Error(B2CError(msg: "Wrong credentials."))
    False -> Ok(SelfAsserted(cookies: response.get_cookies(resp)))
  }
}

fn confirm(
  self_asserted: SelfAsserted,
  auth_info: AuthInfo,
) -> Result(Confirmed, B2CError) {
  let csrf_cookie_key = "x-ms-cpim-csrf"

  try csrf_cookie =
    list.key_find(auth_info.cookies, csrf_cookie_key)
    |> b2c_error("CSRF cookie not found in auth info.")
  let cookies = [#(csrf_cookie_key, csrf_cookie), ..self_asserted.cookies]

  let req = build_req(confirm_url(), http.Get)

  let req =
    list.fold(
      cookies,
      req,
      fn(acc, cookie) { request.set_cookie(acc, cookie.0, cookie.1) },
    )
    |> request.set_query([
      #("csrf_token", auth_info.csrf_token),
      ..base_query(auth_info)
    ])

  try resp =
    req
    |> finch.build([])
    |> finch.request(config.finch_server())
    |> b2c_error("Confirm HTTP request failed.")

  try resp = case resp.status {
    302 -> Ok(resp)
    _ -> Error(B2CError(msg: "Confirm HTTP request bad error code."))
  }

  try location =
    response.get_header(resp, "location")
    |> b2c_error("Location not found for confirm response.")

  try code =
    location
    |> string.split("code=")
    |> list.at(1)
    |> b2c_error("Confirmation code not found.")

  Ok(Confirmed(code: code))
}

fn get_tokens(
  confirmed: Confirmed,
  code_challenge: String,
) -> Result(Tokens, B2CError) {
  let request_token_data = [
    #("code", confirmed.code),
    #("code_verifier", code_challenge),
    #("grant_type", "authorization_code"),
    ..base_request_data()
  ]

  let req =
    build_req(get_token_url(), http.Post)
    |> request.set_body(uri_helpers.form_urlencoded_serialize(
      request_token_data,
    ))

  try resp = run_req(req)
  try data =
    json.decode(resp.body, using: dynamic.dynamic)
    |> b2c_error("Get tokens JSON parsing error: " <> string.inspect(resp.body))

  try token = data_get(data, "access_token", dynamic.string)
  try expires_in = data_get(data, "expires_in", dynamic.int)
  try refresh_token = data_get(data, "refresh_token", dynamic.string)
  try refresh_token_expires_in =
    data_get(data, "refresh_token_expires_in", dynamic.int)

  Ok(Tokens(
    access_token: token,
    access_token_expires_in: expires_in,
    refresh_token: refresh_token,
    refresh_token_expires_in: refresh_token_expires_in,
  ))
}

fn hash_challenge(challenge: String) -> String {
  let hashed = crypto.hash(crypto.Sha256, challenge)
  base.url_encode64(hashed, False)
}

fn authorize_url() -> uri.Uri {
  let url = config.api_url("b2c_auth_url")
  uri.Uri(..url, path: url.path <> "/oauth2/v2.0/authorize")
}

fn self_asserted_url() -> uri.Uri {
  let url = config.api_url("b2c_auth_url")
  uri.Uri(..url, path: url.path <> "/SelfAsserted")
}

fn confirm_url() -> uri.Uri {
  let url = config.api_url("b2c_auth_url")
  uri.Uri(..url, path: url.path <> "/api/CombinedSigninAndSignup/confirmed")
}

fn get_token_url() -> uri.Uri {
  let url = config.api_url("b2c_auth_url")
  uri.Uri(..url, path: url.path <> "/oauth2/v2.0/token")
}

fn build_req(url: uri.Uri, method: http.Method) -> request.Request(String) {
  assert Ok(req) = request.from_uri(url)

  let req = request.set_method(req, method)

  case method {
    http.Post ->
      request.set_header(
        req,
        "content-type",
        "application/x-www-form-urlencoded; charset=UTF-8",
      )
    _ -> req
  }
}

fn run_req(
  req: request.Request(String),
) -> Result(response.Response(String), B2CError) {
  try resp =
    req
    |> finch.build([])
    |> finch.request(config.finch_server())
    |> b2c_error("HTTP request failed.")

  case resp.status {
    200 -> Ok(resp)
    code ->
      Error(B2CError(
        msg: "Not OK response code " <> int.to_string(code) <> " from URL " <> uri.to_string(request.to_uri(
          req,
        )) <> "\n\n" <> string.inspect(resp),
      ))
  }
}

fn base_request_data() -> List(#(String, String)) {
  let b2c_client_id = config.str("b2c_client_id")

  [
    #("client_id", b2c_client_id),
    #("scope", b2c_client_id),
    #("redirect_uri", config.str("b2c_redirect_url")),
  ]
}

fn base_query(auth_info: AuthInfo) -> List(#(String, String)) {
  [
    #("tx", "StateProperties=" <> auth_info.state_code),
    #("p", "B2C_1A_SignUpOrSigninOnline"),
  ]
}

fn data_get(
  data: dynamic.Dynamic,
  key: String,
  data_type: dynamic.Decoder(a),
) -> Result(a, B2CError) {
  parsing.data_get(data, key, data_type, B2CError)
}

fn b2c_error(r: Result(a, b), msg: String) -> Result(a, B2CError) {
  result.replace_error(r, B2CError(msg))
}
