import gleam/dynamic
import gleam/result
import gleam/string

/// Get field from a dynamic data presumed to be a map, or fail with error
pub fn data_get(
  data: dynamic.Dynamic,
  key: String,
  data_type: dynamic.Decoder(a),
  error_constructor: fn(String) -> b,
) -> Result(a, b) {
  data
  |> dynamic.field(key, data_type)
  |> result.replace_error(error_constructor(
    "Field " <> key <> " of correct type not found in data: " <> string.inspect(
      data,
    ),
  ))
}
