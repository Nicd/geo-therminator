import gleam/erlang/atom
import gleam/dynamic
import gleam/uri
import helpers/application

pub fn finch_server() -> atom.Atom {
  atom.create_from_string("Elixir.GeoTherminator.PumpAPI.HTTP")
}

pub fn api_timeout() -> Int {
  let timeout =
    application.fetch_env_angry(
      app_name(),
      atom.create_from_string("api_timeout"),
    )

  assert Ok(timeout_int) = dynamic.int(timeout)
  timeout_int
}

pub fn api_url(config_key: String) -> uri.Uri {
  map_api_url(config_key, fn(s) { s })
}

pub fn map_api_url(config_key: String, mapper: fn(String) -> String) -> uri.Uri {
  let url =
    application.fetch_env_angry(app_name(), atom.create_from_string(config_key))

  assert Ok(url_str) = dynamic.string(url)
  let url_str = mapper(url_str)
  assert Ok(parsed_url) = uri.parse(url_str)
  parsed_url
}

pub fn str(config_key: String) -> String {
  let val =
    application.fetch_env_angry(app_name(), atom.create_from_string(config_key))
  assert Ok(val_str) = dynamic.string(val)
  val_str
}

pub fn int(config_key: String) -> Int {
  let val =
    application.fetch_env_angry(app_name(), atom.create_from_string(config_key))
  assert Ok(val_int) = dynamic.int(val)
  val_int
}

fn app_name() -> atom.Atom {
  assert Ok(name) = atom.from_string("geo_therminator")
  name
}
