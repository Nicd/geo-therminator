import gleam
import gleam/erlang/atom.{Atom}
import gleam/dynamic

pub type FromDynamicError {
  NotAString
  ParseError
}

pub external type DateTime

pub type DateTimeFromISO8601Result {
  Ok(datetime: DateTime, offset: Int)
  Error(reason: Atom)
}

pub fn from_dynamic_iso8601(
  data: dynamic.Dynamic,
) -> Result(#(DateTime, Int), List(dynamic.DecodeError)) {
  try data_str = dynamic.string(data)

  case from_iso8601(data_str) {
    Ok(datetime, offset) -> gleam.Ok(#(datetime, offset))
    Error(_reason) ->
      gleam.Error([
        dynamic.DecodeError(
          expected: "ISO-8601 formatted datetime with timezone",
          found: data_str,
          path: [],
        ),
      ])
  }
}

pub external fn from_iso8601(String) -> DateTimeFromISO8601Result =
  "Elixir.DateTime" "from_iso8601"

pub external fn from_unix(Int) -> Result(DateTime, #(Atom, Atom)) =
  "Elixir.DateTime" "from_unix"

pub external fn utc_now() -> DateTime =
  "Elixir.DateTime" "utc_now"

pub external fn to_unix(DateTime) -> Int =
  "Elixir.DateTime" "to_unix"
