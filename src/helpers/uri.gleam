import gleam/string
import gleam/string_builder
import gleam/list
import gleam/uri

const not_encoded_by_gleam = [
  #("!", "%21"),
  #("$", "%24"),
  #("'", "%27"),
  #("(", "%28"),
  #(")", "%29"),
  #("+", "%2B"),
  #("~", "%7E"),
]

/// x-www-form-urlencoded encoder, follows the spec defined in
/// https://url.spec.whatwg.org/#application/x-www-form-urlencoded
pub fn form_urlencoded_serialize(data: List(#(String, String))) -> String {
  data
  |> list.map(fn(item) {
    let #(key, value) = item
    serialize_value(key)
    |> string_builder.append("=")
    |> string_builder.append_builder(serialize_value(value))
  })
  |> string_builder.join("&")
  |> string_builder.to_string()
}

fn serialize_value(value: String) -> string_builder.StringBuilder {
  value
  |> string.split(" ")
  |> list.map(fn(part) {
    let encoded = uri.percent_encode(part)

    list.fold(
      not_encoded_by_gleam,
      encoded,
      fn(acc, item) {
        let #(char, replacement) = item
        string.replace(acc, char, replacement)
      },
    )
    |> string_builder.from_string()
  })
  |> string_builder.join("+")
}
