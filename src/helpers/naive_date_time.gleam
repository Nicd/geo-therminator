import gleam/dynamic
import gleam/erlang/atom.{Atom}
import gleam/result

pub external type NaiveDateTime

pub fn from_dynamic_iso8601(
  data: dynamic.Dynamic,
) -> Result(NaiveDateTime, List(dynamic.DecodeError)) {
  try data_str = dynamic.string(data)
  from_iso8601(data_str)
  |> result.replace_error([
    dynamic.DecodeError(
      expected: "ISO-8601 formatted naive datetime",
      found: data_str,
      path: [],
    ),
  ])
}

pub external fn from_iso8601(String) -> Result(NaiveDateTime, Atom) =
  "Elixir.NaiveDateTime" "from_iso8601"
