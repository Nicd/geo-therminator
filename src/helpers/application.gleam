import gleam/erlang/atom.{Atom}
import gleam/dynamic.{Dynamic}

pub external fn get_env(Atom, Atom) -> Dynamic =
  "Elixir.Application" "get_env"

pub external fn fetch_env_angry(Atom, Atom) -> Dynamic =
  "Elixir.Application" "fetch_env!"
