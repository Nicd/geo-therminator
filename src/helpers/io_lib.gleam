import gleam/erlang/charlist.{Charlist}

pub external fn format_int(format: String, data: List(Int)) -> Charlist =
  "io_lib" "format"
