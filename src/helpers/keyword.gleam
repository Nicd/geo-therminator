import gleam/erlang/atom.{Atom}

pub external type Keyword

pub external fn init() -> Keyword =
  "Elixir.Keyword" "new"

pub external fn put_int(data: Keyword, key: Atom, value: Int) -> Keyword =
  "Elixir.Keyword" "put"

pub external fn put_string(data: Keyword, key: Atom, value: String) -> Keyword =
  "Elixir.Keyword" "put"
