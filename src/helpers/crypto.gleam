pub type HashAlgorithm {
  // For now, only SHA256
  Sha256
}

pub external fn hash(algo: HashAlgorithm, data: String) -> BitString =
  "crypto" "hash"
