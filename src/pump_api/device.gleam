import gleam/set
import helpers/naive_date_time.{NaiveDateTime}
import helpers/date_time.{DateTime}

pub type Device {
  Device(
    id: Int,
    device_id: Int,
    is_online: Bool,
    last_online: NaiveDateTime,
    created_when: NaiveDateTime,
    mac_address: String,
    name: String,
    model: String,
    retailer_access: Int,
  )
}

pub type Status {
  Status(heating_effect: Int, is_heating_effect_set_by_user: Bool)
}

pub type Register {
  Register(name: String, value: Int, timestamp: DateTime)
}

pub type RegisterCollection {
  RegisterCollection(
    outdoor_temp: Register,
    supply_out: Register,
    supply_in: Register,
    desired_supply: Register,
    brine_out: Register,
    brine_in: Register,
    hot_water_temp: Register,
  )
}

pub type Priority {
  HandOperated
  HotWater
  Heating
  ActiveCooling
  Pool
  AntiLegionella
  PassiveCooling
  Standby
  Idle
  Off
  Defrost
  Unknown
}

pub type OpStat {
  OpStat(priority: set.Set(Priority))
}
