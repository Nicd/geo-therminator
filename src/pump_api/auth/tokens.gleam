import helpers/date_time.{DateTime}

pub type Tokens {
  Tokens(
    access_token: String,
    access_token_expiry: DateTime,
    refresh_token: String,
    refresh_token_expiry: DateTime,
  )
}
