import gleam/http/request
import gleam/result
import gleam/dynamic
import gleam/list
import gleam/string
import pump_api/auth/user.{User}
import pump_api/auth/tokens.{Tokens}
import pump_api/auth/installation_info.{InstallationInfo}
import pump_api/http
import pump_api/api.{ApiError, AuthError, InvalidData}
import helpers/config
import helpers/date_time
import helpers/parsing
import azure/b2c

pub fn auth(username: String, password: String) -> Result(User, ApiError) {
  try tokens =
    b2c.authenticate(username, password)
    |> result.map_error(fn(err) { AuthError(inner: err) })
  try access_token_expiry =
    date_time.from_unix(
      date_time.to_unix(date_time.utc_now()) + tokens.access_token_expires_in,
    )
    |> result.replace_error(InvalidData(
      msg: "Access token expiry could not be converted into DateTime: " <> string.inspect(
        tokens.access_token_expires_in,
      ),
    ))
  try refresh_token_expiry =
    date_time.from_unix(
      date_time.to_unix(date_time.utc_now()) + tokens.refresh_token_expires_in,
    )
    |> result.replace_error(InvalidData(
      msg: "Refresh token expiry could not be converted into DateTime: " <> string.inspect(
        tokens.refresh_token_expires_in,
      ),
    ))

  Ok(User(tokens: Tokens(
    access_token: tokens.access_token,
    access_token_expiry: access_token_expiry,
    refresh_token: tokens.refresh_token,
    refresh_token_expiry: refresh_token_expiry,
  )))
}

pub fn installation_info(user: User) -> Result(List(InstallationInfo), ApiError) {
  let url = config.api_url("api_installations_url")
  assert Ok(raw_req) = request.from_uri(url)

  let empty_req = request.set_body(raw_req, http.Empty)
  try data =
    http.run_json_req(http.authed_req(user, empty_req))
    |> result.replace_error(InvalidData(
      msg: "Could not parse InstallationInfo JSON.",
    ))

  try items =
    parsing.data_get(
      data,
      "items",
      dynamic.list(of: dynamic.field("id", dynamic.int)),
      InvalidData,
    )

  Ok(list.map(items, fn(id) { InstallationInfo(id: id) }))
}
