import pump_api/auth/tokens

pub type User {
  User(tokens: tokens.Tokens)
}
