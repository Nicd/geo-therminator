import azure/b2c.{B2CError}
import pump_api/http

pub type ApiError {
  RequestFailed
  NotOkResponse
  InvalidData(msg: String)
  AuthError(inner: B2CError)
}

pub fn from_http(err: http.ApiError) -> ApiError {
  case err {
    http.RequestFailed -> RequestFailed
    http.NotOkResponse -> NotOkResponse
    http.InvalidData -> InvalidData("Invalid data in HTTP response.")
  }
}
