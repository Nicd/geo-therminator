import gleam/http/request
import gleam/json.{Json}
import gleam/result
import gleam/dynamic
import pump_api/auth/user.{User}
import helpers/config
import finch

pub type Body {
  Empty
  Json(data: Json)
  String(data: String)
}

pub type ApiRequest =
  request.Request(Body)

pub type ApiError {
  RequestFailed
  NotOkResponse
  InvalidData
}

pub fn authed_req(user: User, r: ApiRequest) {
  r
  |> request.set_header("authorization", "Bearer " <> user.tokens.access_token)
  |> req()
}

pub fn req(r: ApiRequest) -> request.Request(String) {
  let r = request.set_header(r, "accept", "application/json")

  case r.body {
    Empty -> request.set_body(r, "")
    String(data) -> request.set_body(r, data)
    Json(data) ->
      r
      |> request.set_header("content-type", "application/json")
      |> request.set_body(json.to_string(data))
  }
}

pub fn run_req(req: request.Request(String)) {
  try resp =
    req
    |> finch.build([])
    |> finch.request(config.finch_server())
    |> result.replace_error(RequestFailed)

  case resp.status {
    200 -> Ok(resp.body)
    _ -> Error(NotOkResponse)
  }
}

pub fn run_json_req(req: request.Request(String)) {
  try body = run_req(req)

  body
  |> json.decode(using: dynamic.dynamic)
  |> result.replace_error(InvalidData)
}
