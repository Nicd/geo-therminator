import gleam/http/request
import gleam/http as gleam_http
import gleam/string
import gleam/int
import gleam/dynamic
import gleam/result
import gleam/list
import gleam/map
import gleam/set
import gleam/json
import pump_api/auth/user.{User}
import pump_api/auth/installation_info.{InstallationInfo}
import pump_api/device.{
  Device, OpStat, Priority, Register, RegisterCollection, Status,
}
import pump_api/http
import pump_api/api.{ApiError, InvalidData}
import helpers/config
import helpers/parsing
import helpers/naive_date_time
import helpers/date_time

const opstat_bitmask_mapping = [
  #(1, device.HandOperated),
  #(2, device.Defrost),
  #(4, device.HotWater),
  #(8, device.Heating),
  #(16, device.ActiveCooling),
  #(32, device.Pool),
  #(64, device.AntiLegionella),
  #(128, device.PassiveCooling),
  #(512, device.Standby),
  #(1024, device.Idle),
  #(2048, device.Off),
]

pub fn device_info(
  user: User,
  installation: InstallationInfo,
) -> Result(Device, ApiError) {
  let url =
    config.map_api_url(
      "api_device_url",
      fn(url) { string.replace(url, "{id}", int.to_string(installation.id)) },
    )
  assert Ok(raw_req) = request.from_uri(url)

  let empty_req = request.set_body(raw_req, http.Empty)
  try data =
    http.run_json_req(http.authed_req(user, empty_req))
    |> result.map_error(api.from_http)

  try last_online =
    parsing.data_get(
      data,
      "lastOnline",
      naive_date_time.from_dynamic_iso8601,
      InvalidData,
    )
  try created_when =
    parsing.data_get(
      data,
      "createdWhen",
      naive_date_time.from_dynamic_iso8601,
      InvalidData,
    )
  try id = parsing.data_get(data, "id", dynamic.int, InvalidData)
  try device_id = parsing.data_get(data, "deviceId", dynamic.int, InvalidData)
  try is_online = parsing.data_get(data, "isOnline", dynamic.bool, InvalidData)
  try mac_address =
    parsing.data_get(data, "macAddress", dynamic.string, InvalidData)
  try name = parsing.data_get(data, "name", dynamic.string, InvalidData)
  try model = parsing.data_get(data, "model", dynamic.string, InvalidData)
  try retailer_access =
    parsing.data_get(data, "retailerAccess", dynamic.int, InvalidData)

  Ok(Device(
    id: id,
    device_id: device_id,
    is_online: is_online,
    last_online: last_online,
    created_when: created_when,
    mac_address: mac_address,
    name: name,
    model: model,
    retailer_access: retailer_access,
  ))
}

pub fn status(user: User, device: Device) {
  let url =
    config.map_api_url(
      "api_device_status_url",
      fn(url) { string.replace(url, "{id}", int.to_string(device.id)) },
    )
  assert Ok(raw_req) = request.from_uri(url)

  let empty_req = request.set_body(raw_req, http.Empty)
  try data =
    http.run_json_req(http.authed_req(user, empty_req))
    |> result.map_error(api.from_http)

  try heating_effect =
    parsing.data_get(data, "heatingEffect", dynamic.int, InvalidData)
  try is_heating_effect_set_by_user =
    parsing.data_get(
      data,
      "isHeatingEffectSetByUser",
      dynamic.bool,
      InvalidData,
    )

  Ok(Status(
    heating_effect: heating_effect,
    is_heating_effect_set_by_user: is_heating_effect_set_by_user,
  ))
}

pub fn register_info(user: User, device: Device) {
  let url =
    config.map_api_url(
      "api_device_register_url",
      fn(url) { string.replace(url, "{id}", int.to_string(device.id)) },
    )
  assert Ok(raw_req) = request.from_uri(url)

  let empty_req = request.set_body(raw_req, http.Empty)
  try data =
    http.run_json_req(http.authed_req(user, empty_req))
    |> result.map_error(api.from_http)

  try registers =
    dynamic.list(parse_register)(data)
    |> result.map_error(fn(err) {
      InvalidData("Unable to parse registers: " <> string.inspect(err))
    })
  let registers_map =
    registers
    |> list.map(fn(r) { #(r.name, r) })
    |> map.from_list()

  try outdoor_temp = get_register(registers_map, "REG_OUTDOOR_TEMPERATURE")
  try supply_out = get_register(registers_map, "REG_SUPPLY_LINE")
  try supply_in = get_register(registers_map, "REG_OPER_DATA_RETURN")
  try desired_supply =
    get_register(registers_map, "REG_DESIRED_SYS_SUPPLY_LINE_TEMP")
  try brine_out = get_register(registers_map, "REG_BRINE_OUT")
  try brine_in = get_register(registers_map, "REG_BRINE_IN")
  try hot_water_temp = get_register(registers_map, "REG_HOT_WATER_TEMPERATURE")

  Ok(RegisterCollection(
    outdoor_temp: outdoor_temp,
    supply_out: supply_out,
    supply_in: supply_in,
    desired_supply: desired_supply,
    brine_out: brine_out,
    brine_in: brine_in,
    hot_water_temp: hot_water_temp,
  ))
}

pub fn opstat(user: User, device: Device) {
  let url =
    config.map_api_url(
      "api_device_opstat_url",
      fn(url) { string.replace(url, "{id}", int.to_string(device.id)) },
    )
  assert Ok(raw_req) = request.from_uri(url)

  let empty_req = request.set_body(raw_req, http.Empty)
  try data =
    http.run_json_req(http.authed_req(user, empty_req))
    |> result.map_error(api.from_http)

  try registers =
    dynamic.list(parse_register)(data)
    |> result.map_error(fn(err) {
      InvalidData("Unable to parse registers: " <> string.inspect(err))
    })
  let registers_map =
    registers
    |> list.map(fn(r) { #(r.name, r) })
    |> map.from_list()

  let priority_register =
    get_register(registers_map, "REG_OPERATIONAL_STATUS_PRIO1")
  let priority_register_fallback =
    get_register(registers_map, "REG_OPERATIONAL_STATUS_PRIORITY_BITMASK")

  try priority = case #(priority_register, priority_register_fallback) {
    #(Ok(data), _) -> Ok(opstat_map(data))
    #(_, Ok(data)) -> Ok(opstat_bitmask_map(data))
    _ ->
      Error(InvalidData(
        "Unable to parse opstat: " <> string.inspect(#(
          priority_register,
          priority_register_fallback,
        )),
      ))
  }

  Ok(OpStat(priority: priority))
}

pub fn set_temp(user: User, device: Device, temp: Int) {
  let url =
    config.map_api_url(
      "api_device_opstat_url",
      fn(url) { string.replace(url, "{id}", int.to_string(device.id)) },
    )
  assert Ok(raw_req) = request.from_uri(url)

  let register_index = config.int("api_device_temp_set_reg_index")
  let client_id = config.str("api_device_reg_set_client_id")

  let req =
    raw_req
    |> request.set_method(gleam_http.Post)
    |> request.set_body(http.Json(data: json.object([
      #("registerIndex", json.int(register_index)),
      #("clientUuid", json.string(client_id)),
      #("registerValue", json.int(temp)),
    ])))

  http.run_req(http.authed_req(user, req))
}

fn parse_register(
  item: dynamic.Dynamic,
) -> Result(Register, List(dynamic.DecodeError)) {
  try #(timestamp, _) =
    dynamic.field("timeStamp", date_time.from_dynamic_iso8601)(item)
  try name = dynamic.field("registerName", dynamic.string)(item)
  try value = dynamic.field("registerValue", dynamic.int)(item)

  Ok(Register(timestamp: timestamp, name: name, value: value))
}

fn get_register(
  data: map.Map(String, Register),
  key: String,
) -> Result(Register, ApiError) {
  map.get(data, key)
  |> result.replace_error(InvalidData(
    "Could not find " <> key <> " in data: " <> string.inspect(data),
  ))
}

fn opstat_map(register: Register) {
  let val = case register.value {
    1 -> device.HandOperated
    3 -> device.HotWater
    4 -> device.Heating
    5 -> device.ActiveCooling
    6 -> device.Pool
    7 -> device.AntiLegionella
    8 -> device.PassiveCooling
    98 -> device.Standby
    99 -> device.Idle
    100 -> device.Off
    _ -> device.Unknown
  }

  set.new()
  |> set.insert(val)
}

fn opstat_bitmask_map(register: Register) {
  let priority_set: set.Set(Priority) = set.new()

  list.fold(
    opstat_bitmask_mapping,
    priority_set,
    fn(output, mapping) {
      let #(int, priority) = mapping

      case band(register.value, int) {
        0 -> output
        _ -> set.insert(output, priority)
      }
    },
  )
}

external fn band(i1: Int, i2: Int) -> Int =
  "erlang" "band"
