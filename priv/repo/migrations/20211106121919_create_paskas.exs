defmodule GeoTherminator.Repo.Migrations.CreatePaskas do
  use Ecto.Migration

  def change do
    create table(:paskas) do
      add :name, :string

      timestamps()
    end
  end
end
