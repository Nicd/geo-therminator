defmodule GeoTherminator.MixProject do
  use Mix.Project

  @app :geo_therminator

  def project do
    [
      app: @app,
      version: "0.4.0",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      erlc_paths: [
        "build/dev/erlang/#{@app}/_gleam_artefacts"
      ],
      erlc_include_path: "build/dev/erlang/#{@app}/include",
      archives: [mix_gleam: "~> 0.6.1"],
      compilers:
        if Mix.env() != :test do
          [:gleam]
        else
          []
        end ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      releases: [
        android: [
          include_executables_for: [:unix],
          include_erts: false
        ]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {GeoTherminator.Application, []},
      extra_applications: [
        :logger,
        :runtime_tools,
        :logger,
        :ssl,
        :crypto,
        :sasl,
        :tools,
        :inets
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    deps_list = [
      {:phoenix, "~> 1.6.2"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.17.11"},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_live_dashboard, "~> 0.6"},
      {:esbuild, "~> 0.2", runtime: Mix.env() == :dev},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.18"},
      {:jason, "~> 1.2"},
      {:plug_cowboy, "~> 2.5"},
      {:dotenv_parser, "~> 2.0"},
      {:finch, "~> 0.14.0"},
      {:finch_gleam, "~> 1.0.0"},
      {:desktop, "~> 1.4"},
      {:cubdb, "~> 2.0"},
      {:gleam_stdlib, "~> 0.26"},
      {:gleam_http, "~> 3.1"},
      {:gleam_erlang, "~> 0.18.0"},
      {:gleam_json, "~> 0.5.0"}
    ]

    if Mix.target() in [:android, :ios] do
      deps_list ++ [{:wx, "~> 1.0", hex: :bridge, targets: [:android, :ios]}]
    else
      deps_list
    end
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"],
      "deps.get": ["deps.get", "gleam.deps.get"]
    ]
  end
end
