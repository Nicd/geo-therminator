defmodule GeoTherminator.PumpAPI.Device.PubSub do
  require GeoTherminator.PumpAPI.Auth.InstallationInfo
  require GeoTherminator.PumpAPI.Device

  alias Phoenix.PubSub
  alias GeoTherminator.PumpAPI.Auth.InstallationInfo
  alias GeoTherminator.PumpAPI.Device

  @installation_topic "installation:"

  @spec subscribe_installation(InstallationInfo.t()) :: :ok
  def subscribe_installation(installation) do
    :ok =
      PubSub.subscribe(
        __MODULE__,
        @installation_topic <> to_string(InstallationInfo.record(installation, :id))
      )
  end

  @spec broadcast_device(GeoTherminator.PumpAPI.Device.t()) :: :ok
  def broadcast_device(device) do
    :ok =
      PubSub.broadcast!(
        __MODULE__,
        @installation_topic <> to_string(Device.record(device, :id)),
        {:device, device}
      )
  end

  @spec broadcast_status(
          GeoTherminator.PumpAPI.Device.t(),
          GeoTherminator.PumpAPI.Device.Status.t()
        ) :: :ok
  def broadcast_status(device, status) do
    :ok =
      PubSub.broadcast!(
        __MODULE__,
        @installation_topic <> to_string(Device.record(device, :id)),
        {:status, status}
      )
  end

  @spec broadcast_registers(
          GeoTherminator.PumpAPI.Device.t(),
          GeoTherminator.PumpAPI.Device.RegisterCollection.t()
        ) :: :ok
  def broadcast_registers(device, registers) do
    :ok =
      PubSub.broadcast!(
        __MODULE__,
        @installation_topic <> to_string(Device.record(device, :id)),
        {:registers, registers}
      )
  end

  @spec broadcast_opstat(
          GeoTherminator.PumpAPI.Device.t(),
          GeoTherminator.PumpAPI.Device.OpStat.t()
        ) :: :ok
  def broadcast_opstat(device, opstat) do
    :ok =
      PubSub.broadcast!(
        __MODULE__,
        @installation_topic <> to_string(Device.record(device, :id)),
        {:opstat, opstat}
      )
  end
end
