defmodule GeoTherminator.PumpAPI.Device do
  require Record

  Record.defrecord(:record, :device, [
    :id,
    :device_id,
    :is_online,
    :last_online,
    :created_when,
    :mac_address,
    :name,
    :model,
    :retailer_access
  ])

  @type t :: :pump_api@device.device()

  defmodule Status do
    Record.defrecord(:record, :status, [:heating_effect, :is_heating_effect_set_by_user])

    @type t :: :pump_api@device.status()
  end

  defmodule Register do
    Record.defrecord(:record, :register, [:name, :value, :timestamp])

    @type t :: :pump_api@device.register()
  end

  defmodule RegisterCollection do
    Record.defrecord(:record, :register_collection, [
      :outdoor_temp,
      :supply_out,
      :supply_in,
      :desired_supply,
      :brine_out,
      :brine_in,
      :hot_water_temp
    ])

    @type t :: :pump_api@device.register_collection()
  end

  defmodule OpStat do
    Record.defrecord(:record, :op_stat, [:priority])

    @type t :: :pump_api@device.op_stat()
  end

  @spec get_device_process(GenServer.name(), GeoTherminator.PumpAPI.Auth.InstallationInfo.t()) ::
          {:ok, GenServer.name()} | :error
  def get_device_process(auth_server, installation) do
    case DynamicSupervisor.start_child(
           __MODULE__.Supervisor,
           {__MODULE__.Server,
            %__MODULE__.Server.Options{
              auth_server: auth_server,
              installation: installation
            }}
         ) do
      {:ok, pid} ->
        {:ok, pid}

      {:error, {:already_started, pid}} ->
        {:ok, pid}

      err ->
        IO.inspect(err)
        :error
    end
  end
end
