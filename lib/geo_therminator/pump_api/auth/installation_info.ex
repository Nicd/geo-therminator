defmodule GeoTherminator.PumpAPI.Auth.InstallationInfo do
  require Record

  Record.defrecord(:record, :installation_info, [:id])

  @type t :: record(:record, id: non_neg_integer())
end
