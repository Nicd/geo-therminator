defmodule GeoTherminator.PumpAPI.Auth.Tokens do
  require Record

  Record.defrecord(:record, :tokens, [
    :access_token,
    :access_token_expiry,
    :refresh_token,
    :refresh_token_expiry
  ])

  @type t :: :pump_api@auth@tokens.tokens()
end
