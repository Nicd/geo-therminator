defmodule GeoTherminator.PumpAPI.Auth.User do
  require Record

  Record.defrecord(:record, :user, [:tokens])

  @type t :: :pump_api@auth@user.user()
end
