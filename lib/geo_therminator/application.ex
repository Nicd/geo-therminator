defmodule GeoTherminator.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    db_dir = Application.get_env(:geo_therminator, :db_dir)

    children = [
      {CubDB, name: GeoTherminator.DB, data_dir: db_dir},
      {Finch, name: GeoTherminator.PumpAPI.HTTP},
      {Phoenix.PubSub, name: GeoTherminator.PumpAPI.Device.PubSub},
      {Registry, keys: :unique, name: GeoTherminator.PumpAPI.Device.Registry},
      {DynamicSupervisor, strategy: :one_for_one, name: GeoTherminator.PumpAPI.Device.Supervisor},
      {DynamicSupervisor,
       strategy: :one_for_one, name: GeoTherminator.PumpAPI.Auth.Server.Supervisor},

      # Start the Telemetry supervisor
      GeoTherminatorWeb.Telemetry,
      # Start the PubSub system
      Supervisor.child_spec({Phoenix.PubSub, name: GeoTherminator.PubSub}, id: :phoenix_pubsub),
      # Start the Endpoint (http/https)
      GeoTherminatorWeb.Endpoint,
      {Desktop.Window,
       [
         app: :geo_therminator,
         id: GeoTherminatorWindow,
         url: &GeoTherminatorWeb.Endpoint.url/0
       ]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GeoTherminator.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GeoTherminatorWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
