defmodule GeoTherminatorWeb.Components.MainView do
  require GeoTherminator.PumpAPI.Device
  require GeoTherminator.PumpAPI.Device.Status
  require GeoTherminator.PumpAPI.Device.RegisterCollection
  require GeoTherminator.PumpAPI.Device.OpStat
  require GeoTherminator.PumpAPI.Device.Register

  use GeoTherminatorWeb, :live_component

  alias GeoTherminator.PumpAPI.Device

  @impl true
  def update(assigns, socket) do
    priority_set =
      Device.OpStat.record(assigns.opstat, :priority) |> elem(1) |> Map.keys() |> MapSet.new()

    {:ok,
     assign(socket,
       set_temp: Device.Status.record(assigns.status, :heating_effect),
       set_temp_active: Device.Status.record(assigns.status, :is_heating_effect_set_by_user),
       hot_water_temp:
         Device.Register.record(
           Device.RegisterCollection.record(assigns.registers, :hot_water_temp),
           :value
         ),
       brine_out:
         Device.Register.record(
           Device.RegisterCollection.record(assigns.registers, :brine_out),
           :value
         ),
       brine_in:
         Device.Register.record(
           Device.RegisterCollection.record(assigns.registers, :brine_in),
           :value
         ),
       supply_out:
         Device.Register.record(
           Device.RegisterCollection.record(assigns.registers, :supply_out),
           :value
         ),
       supply_in:
         Device.Register.record(
           Device.RegisterCollection.record(assigns.registers, :supply_in),
           :value
         ),
       outdoor_temp:
         Device.Register.record(
           Device.RegisterCollection.record(assigns.registers, :outdoor_temp),
           :value
         ),
       priority_set: priority_set
     )}
  end
end
