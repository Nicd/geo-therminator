<div class="main-view-component">
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    version="1.1"
    width="484px"
    height="665px"
    viewBox="-0.5 -0.5 484 665"
    style="background-color: rgb(255, 255, 255);"
  >
    <defs>
      <linearGradient x1="0%" y1="0%" x2="0%" y2="100%" id="mx-gradient-f5f5f5-1-b3b3b3-1-s-0">
        <stop offset="0%" style="stop-color: rgb(245, 245, 245); stop-opacity: 1;" />
        <stop offset="100%" style="stop-color: rgb(179, 179, 179); stop-opacity: 1;" />
      </linearGradient>
      <linearGradient x1="0%" y1="0%" x2="0%" y2="100%" id="mx-gradient-dae8fc-1-7ea6e0-1-s-0">
        <stop offset="0%" style="stop-color: rgb(218, 232, 252); stop-opacity: 1;" />
        <stop offset="100%" style="stop-color: rgb(126, 166, 224); stop-opacity: 1;" />
      </linearGradient>
      <linearGradient x1="0%" y1="0%" x2="0%" y2="100%" id="mx-gradient-f8cecc-1-ea6b66-1-s-0">
        <stop offset="0%" style="stop-color: rgb(248, 206, 204); stop-opacity: 1;" />
        <stop offset="100%" style="stop-color: rgb(234, 107, 102); stop-opacity: 1;" />
      </linearGradient>
    </defs>
    <g>
      <rect
        x="0"
        y="144"
        width="200"
        height="520"
        fill="url(#mx-gradient-f5f5f5-1-b3b3b3-1-s-0)"
        stroke="#666666"
        pointer-events="none"
      />
      <rect
        x="60"
        y="194"
        width="80"
        height="80"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        pointer-events="none"
      />
      <rect
        x="90"
        y="464"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="185"
          y="498"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @outdoor_temp %>°C
        </text>
      </g>
      <rect
        x="10"
        y="468"
        width="30"
        height="40"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        pointer-events="none"
      />
      <path
        d="M 30 488 L 53.63 488"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 58.88 488 L 51.88 491.5 L 53.63 488 L 51.88 484.5 Z"
        fill="rgba(0, 0, 0, 1)"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />

      <%= if MapSet.member?(@priority_set, :heating) do %>
        <ellipse
          cx="172"
          cy="289"
          rx="15"
          ry="15"
          fill="#ffff88"
          stroke="#36393d"
          pointer-events="none"
        />
      <% end %>

      <%= if MapSet.member?(@priority_set, :hot_water) do %>
        <ellipse
          cx="100"
          cy="169"
          rx="15"
          ry="15"
          fill="#ffff88"
          stroke="#36393d"
          pointer-events="none"
        />
      <% end %>

      <path
        d="M 10 344 Q 10 344 53.63 344"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 58.88 344 L 51.88 347.5 L 53.63 344 L 51.88 340.5 Z"
        fill="rgba(0, 0, 0, 1)"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <rect
        x="90"
        y="320"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="185"
          y="354"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @set_temp %>°C
        </text>
      </g>
      <rect
        id="dec-tmp-rect"
        class={"pump-btn #{if @set_temp_active, do: "pump-btn-loading"}"}
        x="25"
        y="384"
        width="60"
        height="60"
        rx="9"
        ry="9"
        fill={if @set_temp_active, do: "#ccc", else: "url(#mx-gradient-dae8fc-1-7ea6e0-1-s-0)"}
        stroke="#6c8ebf"
        phx-click="dec_temp"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          id="dec-tmp-text"
          class={"pump-btn #{if @set_temp_active, do: "pump-btn-loading"}"}
          x="55"
          y="424"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="middle"
          phx-click="dec_temp"
        >
          -
        </text>
      </g>
      <rect
        id="inc-tmp-rect"
        class={"pump-btn #{if @set_temp_active, do: "pump-btn-loading"}"}
        x="115"
        y="384"
        width="60"
        height="60"
        rx="9"
        ry="9"
        fill={if @set_temp_active, do: "#ccc", else: "url(#mx-gradient-f8cecc-1-ea6b66-1-s-0)"}
        stroke="#b85450"
        phx-click="inc_temp"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          id="inc-tmp-text"
          class={"pump-btn #{if @set_temp_active, do: "pump-btn-loading"}"}
          x="145"
          y="424"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="middle"
          phx-click="inc_temp"
        >
          +
        </text>
      </g>
      <path
        d="M 50 144 L 50 24 L 480 24"
        fill="none"
        stroke="#b85450"
        stroke-width="4"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 150 144 L 150 84 L 480 84"
        fill="none"
        stroke="#6c8ebf"
        stroke-width="4"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 200 353 L 310 353 L 310 323 L 480 323"
        fill="none"
        stroke="#6c8ebf"
        stroke-width="4"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 200 223 L 310 223 L 310 263 L 480 263"
        fill="none"
        stroke="#b85450"
        stroke-width="4"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 200 614 L 310 614 L 310 584 L 480 584"
        fill="none"
        stroke="#b85450"
        stroke-width="4"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 200 484 L 310 484 L 310 524 L 480 524"
        fill="none"
        stroke="#6c8ebf"
        stroke-width="4"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <rect
        x="368"
        y="560"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="463"
          y="594"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @brine_in %>°C
        </text>
      </g>
      <rect
        x="368"
        y="500"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="463"
          y="534"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @brine_out %>°C
        </text>
      </g>
      <rect
        x="368"
        y="298"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="463"
          y="332"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @supply_in %>°C
        </text>
      </g>
      <rect
        x="368"
        y="239"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="463"
          y="273"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @supply_out %>°C
        </text>
      </g>
      <rect
        x="168"
        y="32"
        width="97"
        height="48"
        fill="rgba(255, 255, 255, 1)"
        stroke="none"
        pointer-events="none"
      />
      <g transform="translate(-0.5 -0.5)">
        <text
          x="263"
          y="66"
          fill="rgba(0, 0, 0, 1)"
          font-family="Helvetica"
          font-size="34px"
          text-anchor="end"
        >
          <%= @hot_water_temp %>°C
        </text>
      </g>
      <rect
        x="269"
        y="284"
        width="40"
        height="10"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        pointer-events="none"
      />
      <rect
        x="210"
        y="314"
        width="40"
        height="10"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        pointer-events="none"
      />
      <rect
        x="210"
        y="284"
        width="40"
        height="10"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        pointer-events="none"
      />
      <rect
        x="220"
        y="274"
        width="80"
        height="60"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        pointer-events="none"
      />
      <path
        d="M 230 326.5 L 230 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 240 326.5 L 240 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 250 326.5 L 250 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 260 326.5 L 260 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 270 326.5 L 270 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 280 326.5 L 280 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 290 326.5 L 290 281.5"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 230 264 Q 220 254 230 249 Q 240 244 230 234"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 250 264 Q 240 254 250 249 Q 260 244 250 234"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 270 264 Q 260 254 270 249 Q 280 244 270 234"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 290 264 Q 280 254 290 249 Q 300 244 290 234"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 70 60 L 140 60 L 140 80 L 90 80 L 90 100 L 70 100 Z"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 105 35 L 125 35 L 125 45 L 115 45 L 115 65 L 105 65 Z"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        transform="rotate(90,115,50)"
        pointer-events="none"
      />
      <path
        d="M 80 102 L 84.71 115.33 C 84.9 115.87 85 116.43 85 117 C 85 118.33 84.47 119.6 83.54 120.54 C 82.6 121.47 81.33 122 80 122 C 78.67 122 77.4 121.47 76.46 120.54 C 75.53 119.6 75 118.33 75 117 C 75 116.43 75.1 115.87 75.29 115.33 Z"
        fill="#dae8fc"
        stroke="#6c8ebf"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 235 506 C 235 497.72 245.07 491 257.5 491 C 263.47 491 269.19 492.58 273.41 495.39 C 277.63 498.21 280 502.02 280 506 L 280 592 C 280 600.28 269.93 607 257.5 607 C 245.07 607 235 600.28 235 592 Z"
        fill="rgba(255, 255, 255, 1)"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
      <path
        d="M 280 506 C 280 514.28 269.93 521 257.5 521 C 245.07 521 235 514.28 235 506"
        fill="none"
        stroke="rgba(0, 0, 0, 1)"
        stroke-miterlimit="10"
        pointer-events="none"
      />
    </g>
  </svg>
</div>
