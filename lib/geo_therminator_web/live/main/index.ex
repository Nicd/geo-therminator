defmodule GeoTherminatorWeb.MainLive.Index do
  require Logger
  use GeoTherminatorWeb, :live_view

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    socket = assign(socket, error: false)

    credentials = CubDB.get(GeoTherminator.DB, :auth_credentials)

    socket =
      if connected?(socket) and is_map(credentials) do
        try_init(socket, credentials["username"], credentials["password"])
      else
        socket
      end

    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_event(evt, val, socket)

  def handle_event("login", values, socket) do
    {:noreply, try_init(socket, values["username"], values["password"])}
  end

  defp try_init(socket, username, password) do
    on_start =
      DynamicSupervisor.start_child(
        GeoTherminator.PumpAPI.Auth.Server.Supervisor,
        {GeoTherminator.PumpAPI.Auth.Server,
         %GeoTherminator.PumpAPI.Auth.Server.Options{
           server_name: GeoTherminator.PumpAPI.Auth.Server,
           username: username,
           password: password
         }}
      )

    case on_start do
      {:ok, _} ->
        CubDB.put(GeoTherminator.DB, :auth_credentials, %{
          "username" => username,
          "password" => password
        })

        viewing_pump = CubDB.get(GeoTherminator.DB, :viewing_pump)

        if viewing_pump != nil do
          push_redirect(socket,
            to: Routes.live_path(socket, GeoTherminatorWeb.MainLive.Pump, viewing_pump)
          )
        else
          push_redirect(socket,
            to: Routes.live_path(socket, GeoTherminatorWeb.MainLive.DeviceList)
          )
        end

      err ->
        Logger.debug("Error starting auth server: #{inspect(err)}")
        assign(socket, error: err)
    end
  end
end
