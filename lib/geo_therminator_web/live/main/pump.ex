defmodule GeoTherminatorWeb.MainLive.Pump do
  require GeoTherminator.PumpAPI.Device
  require GeoTherminator.PumpAPI.Device.Status
  require GeoTherminator.PumpAPI.Device.RegisterCollection
  require GeoTherminator.PumpAPI.Device.OpStat
  require GeoTherminator.PumpAPI.Device.Register
  use GeoTherminatorWeb, :live_view
  alias GeoTherminator.PumpAPI.Auth
  alias GeoTherminator.PumpAPI.Device
  require Logger

  @impl true
  def mount(%{"id" => str_id}, _session, socket) do
    socket =
      with {id, ""} <- Integer.parse(str_id),
           info when info != nil <- Auth.Server.get_installation(Auth.Server, id),
           :ok <- Device.PubSub.subscribe_installation(info),
           {:ok, pid} <- Device.get_device_process(Auth.Server, info),
           device <- Device.Server.get_device(pid),
           status <- Device.Server.get_status(pid),
           registers <- Device.Server.get_registers(pid),
           opstat <- Device.Server.get_opstat(pid) do
        CubDB.put(GeoTherminator.DB, :viewing_pump, str_id)

        assign(socket,
          pid: pid,
          device: device,
          status: status,
          registers: registers,
          opstat: opstat
        )
      else
        err ->
          Logger.debug("EXPLODY #{inspect(err)}")
          assign(socket, pid: nil, device: nil, status: nil, registers: nil, opstat: nil)
      end

    {:ok, socket}
  end

  @impl true
  def handle_event(event, unsigned_params, socket)

  def handle_event("inc_temp", _params, socket) do
    if not Device.Status.record(socket.assigns.status, :is_heating_effect_set_by_user) do
      current = Device.Status.record(socket.assigns.status, :heating_effect)
      _ = Device.Server.set_temp(socket.assigns.pid, current + 1)

      optimistic_status =
        Device.Status.record(
          socket.assigns.status,
          is_heating_effect_set_by_user: true
        )

      {:noreply, assign(socket, status: optimistic_status)}
    else
      {:noreply, socket}
    end
  end

  def handle_event("dec_temp", _params, socket) do
    if not Device.Status.record(socket.assigns.status, :is_heating_effect_set_by_user) do
      current = Device.Status.record(socket.assigns.status, :heating_effect)
      _ = Device.Server.set_temp(socket.assigns.pid, current - 1)

      optimistic_status =
        Device.Status.record(
          socket.assigns.status,
          is_heating_effect_set_by_user: true
        )

      {:noreply, assign(socket, status: optimistic_status)}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info(msg, socket)

  def handle_info({:device, device}, socket) do
    {:noreply, assign(socket, device: device)}
  end

  def handle_info({:status, status}, socket) do
    {:noreply, assign(socket, status: status)}
  end

  def handle_info({:registers, registers}, socket) do
    {:noreply, assign(socket, registers: registers)}
  end

  def handle_info({:opstat, opstat}, socket) do
    {:noreply, assign(socket, opstat: opstat)}
  end

  def handle_info(msg, socket) do
    Logger.debug("Unknown message: #{inspect(msg)}")
    {:noreply, socket}
  end
end
