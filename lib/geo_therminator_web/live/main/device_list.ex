defmodule GeoTherminatorWeb.MainLive.DeviceList do
  require GeoTherminator.PumpAPI.Auth.InstallationInfo
  use GeoTherminatorWeb, :live_view
  alias GeoTherminator.PumpAPI.Auth.InstallationInfo

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    CubDB.delete(GeoTherminator.DB, :viewing_pump)

    user = GeoTherminator.PumpAPI.Auth.Server.get_auth(GeoTherminator.PumpAPI.Auth.Server)

    installations =
      GeoTherminator.PumpAPI.Auth.Server.get_installations(GeoTherminator.PumpAPI.Auth.Server)

    {:ok, assign(socket, user: user, installations: installations)}
  end

  @impl Phoenix.LiveView
  def handle_event(evt, value, socket)

  def handle_event("logout", _value, socket) do
    DynamicSupervisor.stop(GeoTherminator.PumpAPI.Auth.Server.Supervisor)
    CubDB.delete(GeoTherminator.DB, :auth_credentials)

    {:noreply,
     Phoenix.LiveView.push_redirect(socket,
       to: Routes.live_path(socket, GeoTherminatorWeb.MainLive.Index)
     )}
  end
end
